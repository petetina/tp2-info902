package com.company;

import com.company.Message.BroadcastMessage;
import com.company.Message.DedicatedMessage;
import com.company.Message.Message;
import com.company.SystemMessage.SynchronizedMessage;
import com.company.SystemMessage.TokenMessage;
import com.google.common.eventbus.Subscribe;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;


public class Process  implements Runnable {
    private Thread thread;
    private EventBusService bus;
    private boolean alive;
    private boolean dead;
    private static int nbProcess = 0;
    private int id = Process.nbProcess++;
    private int lamportCounter = 0;
    private int NB_THREADS;
    private String token = null;
    private State state = State.NULL;
    private int synchronizedProcesses = 0;
    private Map<Integer, Integer> map = new HashMap<>();

    public Process(String name, final int nbThreads){

        this.bus = EventBusService.getInstance();
        this.bus.registerSubscriber(this); // Auto enregistrement sur le bus afin que les methodes "@Subscribe" soient invoquees automatiquement.

        this.thread = new Thread(this);
        this.thread.setName(name);
        this.alive = true;
        this.dead = false;
        this.thread.start();

        this.NB_THREADS = nbThreads;
    }

    // Declaration de la methode de callback invoquee lorsqu'un message de type Bidule transite sur le bus
    @Subscribe
    public void onTrucSurBus(Message m){
        //System.out.println(Thread.currentThread().getName() + " receives: " + ((Bidule)m.getPayload()).getMachin() + " for " + this.thread.getName() + " with stamping " + m.getStamping());
        this.lamportCounter = Math.max(m.getStamping(),this.lamportCounter) + 1;
        //System.out.println("stamping after increment = " + this.lamportCounter + " for " + this.thread.getName());
    }

    /*
    //-------------- ON BROADCAST FOR BIDULE --------------
    @Subscribe
    public void onBroadcast(BroadcastMessage m){
        if(m.getSourceId() != id){
            this.lamportCounter = Math.max(m.getStamping(),this.lamportCounter) + 1;
            //System.out.println(Thread.currentThread().getName() + " receives broadcast : " +
            //        ((Bidule)m.getPayload()).getMachin() + " for " + this.thread.getName());
            //System.out.println(Thread.currentThread().getName() + " increment stamping to " + this.lamportCounter);
        }
    }
    */

    @Subscribe
    public void onBroadcast(BroadcastMessage m){
        if(m.getSourceId() != id){
            this.lamportCounter = Math.max(m.getStamping(),this.lamportCounter) + 1;
            //Save the number received
            map.put(m.getSourceId(),(int)m.getPayload());
        }
    }

    @Subscribe
    public void onReceive(DedicatedMessage m){
        //System.out.println("onReceive receiverId = " + m.getReceiverId() + " and id = " + id + ", so condition = " + (m.getReceiverId() == id));
        if(m.getReceiverId() == id){
            System.out.println(Thread.currentThread().getName() + " receives dedicated message : " +
                    ((Bidule)m.getPayload()).getMachin() + " for " + this.thread.getName() + "with stamping " + m.getStamping());
            this.lamportCounter = Math.max(m.getStamping(),this.lamportCounter) + 1;
//            System.out.println(Thread.currentThread().getName() + " increment stamping to " + this.lamportCounter);
        }
    }

    public void run(){
        int loop = 0;
        int maxLoop = 1;

        System.out.println(Thread.currentThread().getName() + " id :" + this.id);

        /*TEST FINAL*/

        //While présent pour la lisibilite
        while(this.alive && loop < maxLoop){
            //System.out.println(Thread.currentThread().getName() + " Loop : " + loop);
            try{
                int number = 1 + (int)(Math.random() * 6);
                System.out.println(id + " choose number " + number);
                int waitRandom = 1 + (int)(Math.random() * 3) * 1000;
                thread.sleep(waitRandom);
                map.put(id, number);
                broadcastMessage(number);

                waitRandom = 1 + (int)(Math.random() * 3) * 1000;
                thread.sleep(waitRandom);
                //Attend que tous les résultats soient connus
                synchronize();

                //On prend le max des résultats
                int processMax = -1;
                int maxValue = -1;
                for(int i=0; i<NB_THREADS; i++){
                    if(processMax == -1 || maxValue < map.get(i))
                    {
                        processMax = i;
                        maxValue = map.get(i);
                    }
                }

                System.out.println(id + ":Process chosen : " + processMax + " with value : " + maxValue);

                if(id == processMax){
                   request();
                   System.out.println(id + " writing result in file..");
                    PrintWriter writer = new PrintWriter("message.txt", "UTF-8");
                    writer.print(id + ":");
                    writer.println(maxValue);
                    writer.close();
                   release();
                }

                synchronize();

                /*TEST ENVOI DE MESSAGE*/

                /*Thread.sleep(500);


                if(id != NB_THREADS - 1) {
                    //if(Thread.currentThread().getName().equals("P1")){
                    request();


                    System.out.println("DO SOMETHING !!!!!!");
                    thread.sleep(500);
                    //Bidule b1 = new Bidule("ga");
                    //Bidule b2 = new Bidule("bu");
                    // postMessage(b1);
                    // broadcastMessage(b2);
                    //sendTo(b1,next());
                    release();

                    //}
                }*/

                /*TEST SYNCHRONIZE */
//                System.out.println(id + " sleep " + ((2 * id * 1000)+2000) + "ms");
//                thread.sleep((2 * id * 1000) + 2000);
//                synchronize();
//                System.out.println(id + " is synchronized !");

            }catch(Exception e){
                e.printStackTrace();
            }
            loop++;
        }

        // liberation du bus
        this.bus.unRegisterSubscriber(this);
        this.bus = null;
        System.out.println(Thread.currentThread().getName() + " stoped");
        this.dead = true;
    }

    private void postMessage(Object payload){
        this.lamportCounter++;
//        System.out.println(Thread.currentThread().getName() + " increment stamping to " + this.lamportCounter);
        Message m = new Message(payload, lamportCounter);
        System.out.println(Thread.currentThread().getName() + " send : " + ((Bidule)payload).getMachin() + " with stamping " + lamportCounter);
        bus.postEvent(m);
    }

    /*
    //------------- BROADCAST FOR BIDULE -----------------
    public void broadcastMessage(Object payload){
        this.lamportCounter++;
        System.out.println(Thread.currentThread().getName() + " increment stamping to " + this.lamportCounter);
        BroadcastMessage m = new BroadcastMessage(payload, id, lamportCounter);
        System.out.println(Thread.currentThread().getName() + " broadcast : " + ((Bidule)payload).getMachin());
        bus.postEvent(m);
    }
    */

    public void broadcastMessage(Object payload){
        this.lamportCounter++;
//        System.out.println(Thread.currentThread().getName() + " increment stamping to " + this.lamportCounter);
        BroadcastMessage m = new BroadcastMessage(payload, id, lamportCounter);
        System.out.println(Thread.currentThread().getName() + " broadcast : " + ((int)payload));
        bus.postEvent(m);
    }

    public void sendTo(Object payload, int to){
        this.lamportCounter++;
//        System.out.println(Thread.currentThread().getName() + " increment stamping to " + this.lamportCounter);
        DedicatedMessage m = new DedicatedMessage(payload, to, lamportCounter);

        System.out.println(Thread.currentThread().getName() + " send dedicated message : " + ((Bidule)payload).getMachin() + " to " + to);
        bus.postEvent(m);
    }

    public void waitStoped(){
        while(!this.dead){
            try{
                Thread.sleep(500);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    public void stop(){
        this.alive = false;
    }

    private int next(){
        return (id + 1)%NB_THREADS;
    }

    public void sendToken(String token) {
        if(!dead) {
            TokenMessage m = new TokenMessage(token, next());
            //System.out.println(id + " sendToken to " + next());
            bus.postEvent(m);
            this.token = null;
        }
    }

    @Subscribe
    public void onToken(TokenMessage m){
        if(m.getReceiverId() == id) {
            //System.out.println(id + " waiting token");

            this.token = m.getToken();
            //System.out.println(id + " receive token with state = " + state.name());
            if(state.equals(State.REQUEST)) {
                state = State.SC;
                while (state.equals(State.SC)) {
                    //System.out.println(id + " is in SC state and does something !");
                    //System.out.flush();
                    //DO nothing
                    try {
                        thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                sendToken(this.token);
                state = State.NULL;
            }else {
                sendToken(token);
            }
        }
        try {
            thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void request() throws Exception {
        this.state = State.REQUEST;
        //System.out.println("request : " + id + " get state = " + state.name());
        //Attend qu'il ait le token pour aller en SC
        while (state.equals(State.REQUEST)) {
            thread.sleep(500);
        }
    }
    private void release() throws Exception{
        this.state = State.RELEASE;
        //Pour pas qu'il y ait de famine
        while (state.equals(State.NULL))
            thread.sleep(1000);
        //System.out.println("release : " + id + " state = " + state.name());

    }

    private void synchronize() throws Exception{
        boolean isSynchroDone = false;
        SynchronizedMessage message = new SynchronizedMessage(id);
        System.out.println(id + " send synchronization message !");
        bus.postEvent(message);
        while(!isSynchroDone)
        {
            thread.sleep(1000);
            System.out.println(id + " waiting for synchronization, synchronizedProcesses = " + synchronizedProcesses);
            if(synchronizedProcesses >= NB_THREADS - 1)
            {
                System.out.println(id + " synchronizedProcesses = " + synchronizedProcesses + ", end synchronize ");
                isSynchroDone = true;
                //S'il y a plusieurs synchronize de suite
                synchronizedProcesses -= (NB_THREADS - 1);
            }
        }
    }

    @Subscribe
    private void onSynchronize(SynchronizedMessage message){
        if(message.getSourceId() != id){
            System.out.println(id + " receive synchro message from " + message.getSourceId() + " !");
            synchronizedProcesses++;
        }
    }

}
