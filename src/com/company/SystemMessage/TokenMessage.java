package com.company.SystemMessage;

public class TokenMessage {
    private String token;
    private int receiverId;

    public TokenMessage(String token, int receiverId) {
        this.token = token;
        this.receiverId = receiverId;
    }

    public String getToken(){
        return token;
    }

    public int getReceiverId(){
        return receiverId;
    }
}
