package com.company;

public class Launcher{

    public static void main(String[] args){

        final int NB_THREADS = 3;
        Process p1 = new Process("P1", NB_THREADS);
        Process p2 = new Process("P2", NB_THREADS);
        Process p3 = new Process("P3", NB_THREADS);

        p1.sendToken("a");
        try{
            Thread.sleep(10000);
        }catch(Exception e){
            e.printStackTrace();
        }

        p1.stop();
        p2.stop();
        p3.stop();
    }
}

