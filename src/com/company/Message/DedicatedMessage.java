package com.company.Message;

public class DedicatedMessage extends Message {
    private int receiverId;

    public DedicatedMessage(Object payload, int receiverId, int stamping) {
        super(payload, stamping);
        this.receiverId = receiverId;
    }

    public int getReceiverId(){
        return receiverId;
    }

}
