package com.company.Message;

public class Message {
    private int stamping;
    private Object payload;

    public Message(Object payload, int stamping) {
        this.stamping = stamping;
        this.payload = payload;
    }

    public int getStamping() {
        return stamping;
    }

    public Object getPayload() {
        return payload;
    }
}
